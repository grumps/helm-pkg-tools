FROM debian:sid
RUN apt-get update -y \
        && apt-get install -y curl make git \
            && curl -L -o /tmp/hm.tar.gz https://get.helm.sh/helm-v2.14.3-linux-amd64.tar.gz \
                && tar xvf /tmp/hm.tar.gz -C /tmp \
                    && install /tmp/linux-amd64/helm /usr/local/bin/helm

